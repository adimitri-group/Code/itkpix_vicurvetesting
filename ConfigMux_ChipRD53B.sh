#!/bin/bash
#Configure MUX
#ToRun: sh ConfigMux_ChipRD53B.sh $ChipConfigFile 0 1
#!config file set $YarrPath/configs/connectivity/rd53b_SCC.json

baseDir=`dirname $0`
source ${baseDir}/config.sh

cd ${YarrPath}

valueEn=`grep MonitorEn $1 | grep -o '[0-9]*'`
valueI=`grep MonitorI $1 | grep -o '[0-9]*'`
valueV=`grep MonitorV $1 | grep -o '[0-9]*'`

valueIMUX=$2
valueVMUX=$3

sed -i "s/\"MonitorEn\": ${valueEn}/\"MonitorEn\": 1/g" $1
sed -i "s/\"MonitorI\": ${valueI}/\"MonitorI\": ${valueIMUX}/g" $1
sed -i "s/\"MonitorV\": ${valueV}/\"MonitorV\": ${valueVMUX}/g" $1

timeout 10m ./bin/scanConsole -r configs/controller/specCfg-rd53b.json -c ${YarrConnConfig} -n 1 >> logMUX.log

valueI=`grep MonitorI $1 | grep -o '[0-9]*'`
valueV=`grep MonitorV $1 | grep -o '[0-9]*'`

sed -i "s/\"MonitorEn\": 1/\"MonitorEn\": 0/g" $1
sed -i "s/\"MonitorI\":  ${valueI}/\"MonitorI\": 63/g" $1
sed -i "s/\"MonitorV\": ${valueV}/\"MonitorV\": 63/g" $1

cd -

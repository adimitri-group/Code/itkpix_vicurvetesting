#!/bin/bash
#configure all paths
#to run: source config.sh

YarrPath="$HOME/YarrRD53B_test"
labRemotePath="$HOME/labRemoteNew/build"
anaMonitorPath="$HOME/rd53b_anamon_branch"
YarrConnConfig="${YarrPath}/configs/connectivity/rd53b_SCC.json"
chipConfigFile="${YarrPath}/configs/rd53b_Chip_SCC_0x10A83.json"

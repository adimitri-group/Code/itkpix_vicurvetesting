import os
import sys
import time

#Measure VI curve
f = open(sys.argv[1], "a")
#execfile("config.sh")

n = 1.2
while (n > 1.18):
       print(n)
       #set current
       cmd_dig_power='/home/adimitri/labRemoteNew/build/bin/powersupply power-on 2.5 ' + str(n) + ' -n PS2A -e /home/adimitri/labRemoteNew/src/configs/input-hw_washbear.json'
       so_power = os.popen(cmd_dig_power).read()
       time.sleep(5)

       #read voltages
       cmd_dig='/home/adimitri/labRemoteNew/build/bin/powersupply meas-voltage -n PS2A -e /home/adimitri/labRemoteNew/src/configs/input-hw_washbear.json'
       so_dig = os.popen(cmd_dig).read()

       cmd_measureAll = '/home/adimitri/rd53b_anamon_branch/build/bin/ana-mon -c /home/adimitri/rd53b_anamon_branch/config/scc_anamon_config_setup03.json  -r /home/adimitri/rd53b_anamon_branch/config/mon_config_washbear_All.json --no-ps'
       so_measureAll = os.popen(cmd_measureAll).readlines()
       #time.sleep(1)
       #print so_measureAll
       values = so_measureAll[3]
       print values

       #ReadMUX with anamon card
       #os.chdir('../../YarrRD53B_test')
       cmd_measureMUX = '/home/adimitri/rd53b_anamon_branch/build/bin/ana-mon -c /home/adimitri/rd53b_anamon_branch/config/scc_anamon_config_setup03.json  -r /home/adimitri/rd53b_anamon_branch/config/mon_config_washbear_MUX.json --no-ps'
       cmd_MUX_Vina = 'sh ConfigMux_ChipRD53B.sh /home/adimitri/YarrRD53B_test/configs/rd53b_Chip_SCC_0x10A83.json 63 33'
       so_cmd_MUX_Vina = os.popen(cmd_MUX_Vina).read()
       so_measureAll = os.popen(cmd_measureMUX).readlines()
       so_MUX_Vina = so_measureAll[3]
       cmd_MUX_Vind = 'sh ConfigMux_ChipRD53B.sh /home/adimitri/YarrRD53B_test/configs/rd53b_Chip_SCC_0x10A83.json 63 37'
       so_cmd_MUX_Vind = os.popen(cmd_MUX_Vind).read()
       so_measureAll = os.popen(cmd_measureMUX).readlines()
       so_MUX_Vind = so_measureAll[3]
       cmd_MUX_Voff = 'sh ConfigMux_ChipRD53B.sh /home/adimitri/YarrRD53B_test/configs/rd53b_Chip_SCC_0x10A83.json 63 36'
       so_cmd_MUX_Voff = os.popen(cmd_MUX_Voff).read()
       so_measureAll = os.popen(cmd_measureMUX).readlines()
       so_MUX_Voff = so_measureAll[3]
       cmd_MUX_Vdda = 'sh ConfigMux_ChipRD53B.sh /home/adimitri/YarrRD53B_test/configs/rd53b_Chip_SCC_0x10A83.json 63 34'
       so_cmd_MUX_Vdda = os.popen(cmd_MUX_Vdda).read()
       so_measureAll = os.popen(cmd_measureMUX).readlines()
       so_MUX_Vdda = so_measureAll[3]
       cmd_MUX_Vddd = 'sh ConfigMux_ChipRD53B.sh /home/adimitri/YarrRD53B_test/configs/rd53b_Chip_SCC_0x10A83.json 63 38'
       so_cmd_MUX_Vddd = os.popen(cmd_MUX_Vddd).read()
       so_measureAll = os.popen(cmd_measureMUX).readlines()
       so_MUX_Vddd = so_measureAll[3]
       cmd_MUX_IGND = 'sh ConfigMux_ChipRD53B.sh /home/adimitri/YarrRD53B_test/configs/rd53b_Chip_SCC_0x10A83.json 26 1'
       so_cmd_MUX_IGND = os.popen(cmd_MUX_IGND).read()
       so_measureAll = os.popen(cmd_measureMUX).readlines()
       so_MUX_IGND = so_measureAll[3]
       cmd_MUX_IanaI = 'sh ConfigMux_ChipRD53B.sh /home/adimitri/YarrRD53B_test/configs/rd53b_Chip_SCC_0x10A83.json 28 1'
       so_cmd_MUX_IanaI = os.popen(cmd_MUX_IanaI).read()
       so_measureAll = os.popen(cmd_measureMUX).readlines()
       so_MUX_IanaI = so_measureAll[3]
       cmd_MUX_IanaS = 'sh ConfigMux_ChipRD53B.sh /home/adimitri/YarrRD53B_test/configs/rd53b_Chip_SCC_0x10A83.json 29 1'
       so_cmd_MUX_IanaS = os.popen(cmd_MUX_IanaS).read()
       so_measureAll = os.popen(cmd_measureMUX).readlines()
       so_MUX_IanaS = so_measureAll[3]
       cmd_MUX_IdigI = 'sh ConfigMux_ChipRD53B.sh /home/adimitri/YarrRD53B_test/configs/rd53b_Chip_SCC_0x10A83.json 30 1'
       so_cmd_MUX_IdigI = os.popen(cmd_MUX_IdigI).read()
       so_measureAll = os.popen(cmd_measureMUX).readlines()
       so_MUX_IdigI = so_measureAll[3]
       cmd_MUX_IdigS = 'sh ConfigMux_ChipRD53B.sh /home/adimitri/YarrRD53B_test/configs/rd53b_Chip_SCC_0x10A83.json 31 1'
       so_cmd_MUX_IdigS = os.popen(cmd_MUX_IdigS).read()
       so_measureAll = os.popen(cmd_measureMUX).readlines()
       so_MUX_IdigS = so_measureAll[3]
       cmd_MUX_Iref = 'sh ConfigMux_ChipRD53B.sh /home/adimitri/YarrRD53B_test/configs/rd53b_Chip_SCC_0x10A83.json 0 1'
       so_cmd_MUX_Iref = os.popen(cmd_MUX_Iref).read()
       so_measureAll = os.popen(cmd_measureMUX).readlines()
       so_MUX_Iref = so_measureAll[3]
       cmd_MUX_VrefADC = 'sh ConfigMux_ChipRD53B.sh /home/adimitri/YarrRD53B_test/configs/rd53b_Chip_SCC_0x10A83.json 63 0'
       so_cmd_MUX_VrefADC = os.popen(cmd_MUX_VrefADC).read()
       so_measureAll = os.popen(cmd_measureMUX).readlines()
       so_MUX_VrefADC = so_measureAll[3]
       cmd_MUX_GND = 'sh ConfigMux_ChipRD53B.sh /home/adimitri/YarrRD53B_test/configs/rd53b_Chip_SCC_0x10A83.json 63 20'
       so_cmd_MUX_GND = os.popen(cmd_MUX_GND).read()
       so_measureAll = os.popen(cmd_measureMUX).readlines()
       so_MUX_GND = so_measureAll[3]
       #os.chdir("../labRemoteNew/build/")


       f.write("%f %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s \n" % (n, so_dig[:-1], values[:-1], so_MUX_Vina[:-1], so_MUX_Vind[:-1], so_MUX_Voff[:-1], so_MUX_Vdda[:-1], so_MUX_Vddd[:-1], so_MUX_IGND[:-1], so_MUX_IanaI[:-1], so_MUX_IanaS[:-1], so_MUX_IdigI[:-1], so_MUX_IdigS[:-1], so_MUX_Iref[:-1], so_MUX_VrefADC[:-1], so_MUX_GND[:-1]))
       time.sleep(0.5)

       #turn off
       #cmd_powerOff='/home/adimitri/labRemoteNew/build/bin/powersupply power-off -n PS2A -c 1 -e /home/adimitri/labRemoteNew/src/configs/input-hw_washbear.json'
       #so_dig_powerOff = os.popen(cmd_dig_powerOff).read()
       #cmd_ana_powerOff='/home/adimitri/labRemoteNew/build/bin/powersupply power-off -n PS2A -c 2 -e /home/adimitri/labRemoteNew/src/configs/input-hw_washbear.json'
       #so_ana_powerOff = os.popen(cmd_ana_powerOff).read()
       #time.sleep(3)

       n=n-0.05


f.close()
#os.chdir('../../YarrRD53B_test')
